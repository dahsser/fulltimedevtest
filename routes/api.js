const express = require('express');
const router = express.Router();

const productoAPIController = require('../controllers/api/producto');
const unidadMedidaAPIController = require('../controllers/api/unidadMedida');
const tiendaAPIController = require('../controllers/api/tienda');

router.get('/producto/get', productoAPIController.getProductos);
router.get('/producto/get/:idProducto', productoAPIController.getProducto);
router.post('/producto/create', productoAPIController.createProducto);
router.post('/producto/update', productoAPIController.updateProducto);
router.post('/producto/delete', productoAPIController.deleteProducto);

router.get('/unidadMedida/get', unidadMedidaAPIController.getUnidadesMedida);

router.get('/tienda');

module.exports = router;
