$(function () {
	fetch('/api/unidadMedida/get')
		.then(response => {
			if (response.ok) {
				return response.json();
			}
			throw Error(response.statusText);
		})
		.then(unidadesMedida => {
			fillUnidadesMedidaSelect(unidadesMedida);
		})
		.catch(err => {
			alert("Error obteniendo las unidades de medida");
		})
	fetch('/api/producto/get')
		.then(response => {
			if (response.ok) {
				return response.json();
			}
			throw Error(response.statusText);
		})
		.then(productos => {
			fillProductosTabla(productos.productos);
		})
		.catch(err => {
			console.error(err);
			alert("Error obteniendo los productos");
		})
})

function fillUnidadesMedidaSelect(unidadesMedida) {
	console.log(unidadesMedida);
	let umSelect = $('#unidadMedidaSelect');
	let umSelectModal = $('#unidadMedidaSelectModal');
	for (let um of unidadesMedida) {
		let option = $(document.createElement('option'));
		let option2 = $(document.createElement('option'));
		option.attr('value', um.abbreviation);
		option.text(um.description);
		option2.attr('value', um.abbreviation);
		option2.text(um.description);
		umSelect.append(option);
		umSelectModal.append(option2);
	}
}

function htmlProductoRow(producto) {
	return `<tr data-idProducto = "${producto.idProducto}">
						<th scope="row">${producto.index}</th>
						<td>${producto.SKU}</td>
						<td>${producto.nombreProducto}</td>
						<td>${producto.abbreviation}</td>
						<td>${producto.createdAt}</td>
						<td>${producto.estadoProducto == '1' ? 'Activo' : 'Eliminado'}</td>
						<td>
							<button type="button" id="editar-${producto.idProducto}" class="btn btn-outline-info" ${producto.estadoProducto == '1' ? '' : 'disabled'}>Editar</button>
							<button type="button" id="eliminar-${producto.idProducto}" class="btn btn-outline-danger" ${producto.estadoProducto == '1' ? '' : 'disabled'}>Eliminar</button>
						</td>
					</tr>`;
}
function fillProductosTabla(productos) {
	console.log(productos);
	let tbodyProductos = $('#tbodyProductos');
	let cont = 1;
	for (let producto of productos) {
		producto.index = cont++;
		tbodyProductos.append(htmlProductoRow(producto));
	}
	$('button[id^=eliminar]').on('click', function () {
		let idProducto = $(this).attr("id").split("-")[1]
		if (confirm("Está seguro que desea eliminar el producto?")) {
			fetch('/api/producto/delete', {
				method: 'POST',
				body: JSON.stringify({
					idProducto: idProducto
				}),
				headers: {
					'Content-Type': 'application/json'
				}
			})
				.then(response => {
					if (response.ok) {
						return response.json();
					}
					throw Error(response.statusText);
				})
				.then(() => {
					alert("El producto fue eliminado con éxito");
					location.reload();
				})
				.catch(err => {
					alert("Ocurrió un error al eliminar el producto");
				})
		}
	});
	$('button[id^=editar]').on('click', function () {
		let idProducto = $(this).attr("id").split("-")[1];
		$('#modalEdicionProducto').modal('show');
		fetch(`/api/producto/get/${idProducto}`)
			.then(response => {
				if (response.ok) {
					return response.json();
				}
				throw Error(response.statusText);
			})
			.then(producto => {
				$('#skuInputModal').val(producto.SKU);
				$('#unidadMedidaSelectModal').val(producto.abbreviation);
				$('#nombreProductoInputModal').val(producto.nombreProducto);
				$('#idProductoModal').val(producto.idProducto);
			})
			.catch(err => {
				console.log("Falló al conseguir datos del producto:" + err);
			})
	});
}

$("#guardarProductoButton").on('click', function () {
	let skuProducto = $('#skuInput').val().trim();
	let nombreProducto = $('#nombreProductoInput').val().trim();
	let unidadMedida = $('#unidadMedidaSelect').val();
	if (!skuProducto || !nombreProducto || !unidadMedida) {
		alert("Tiene que escribir un SKU, nombre y unidad de medida del producto");
	} else {
		fetch('/api/producto/create', {
			method: 'POST',
			body: JSON.stringify({
				sku: skuProducto,
				nombreProducto: nombreProducto,
				unidadMedida: unidadMedida
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		})
			.then(response => {
				if (response.ok) {
					return response.json();
				}
				throw Error(response.statusText);
			})
			.then(() => {
				alert("Producto creado con éxito");
				location.reload();
			})
			.catch(err => {
				console.log(err);
				alert("Error creando producto: " + err);
			})
	}
});

$('#guardarCambiosModalButton').on('click', function(){
	let sku = $('#skuInputModal').val().trim();
	let unidadMedida = $('#unidadMedidaSelectModal').val();
	let nombreProducto = $('#nombreProductoInputModal').val().trim();
	let idProducto = $('#idProductoModal').val();
	if(!sku || !unidadMedida || !nombreProducto){
		alert("Los campos de SKU, nombre de producto y unidad de medida no pueden quedar vacíos");
	}else{
		fetch('/api/producto/update', {
			method:'POST',
			body: JSON.stringify({
				idProducto: idProducto,
				sku: sku,
				nombreProducto: nombreProducto,
				unidadMedida: unidadMedida
			}),
			headers:{
				'Content-Type': 'application/json'
			}
		})
		.then(response=>{
			if(response.ok){
				return response.json();
			}
			throw Error(response.statusText);
		})
		.then(()=>{
			alert("Producto actualizado con éxito");
			location.reload();
		})
		.catch(err=>{
			console.error(err);
			alert("Hubo un error al actualizar el producto:"+ err)
		})
	}
});

