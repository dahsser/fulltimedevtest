const UnidadMedida = require('../../models/unidadMedida');

exports.getUnidadesMedida = (req, res) => {
	UnidadMedida.getUnidadMedidaAll()
		.then(unidadesMedida => {
			res.json(unidadesMedida);
		})
		.catch(err => {
			console.error(err);
			res.status(500).json({error: true, message:'An internal error has occurred'});
		})
}