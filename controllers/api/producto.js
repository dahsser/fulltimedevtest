const Producto = require('../../models/producto');
const UnidadMedida = require('../../models/unidadMedida');
exports.getProducto = (req, res) => {
	let idProducto = req.params.idProducto || "";

	if (!idProducto) {
		res.status(400).json({ error: false, message: 'Bad request' });
	} else {
		Producto.get(idProducto)
			.then(producto => {
				if (producto.length) {
					producto = producto[0];
					res.json({ error: false, ...producto })
				} else {
					res.json({ error: false, message: 'Product not found.' })
				}
			})
			.catch(err => {
				console.error(err);
				res.status(500).json({ error: true, message: 'An internal error has occurred' });
			})
	}
}

exports.getProductos = (req, res) => {
	Producto.getAll()
		.then(productos => {
			res.json({ error: false, productos: productos });
		})
		.catch(err => {
			console.error(err);
			res.status(500).json({ err: true, err })
		})
}

exports.createProducto = (req, res) => {
	let { sku, nombreProducto, unidadMedida } = req.body;
	console.log(req.body);
	if (!sku || !nombreProducto || !unidadMedida) {
		res.status(400).json({ error: true, message: 'Bad request' });
	} else {
		console.log(unidadMedida);
		UnidadMedida.getUnidadMedida(unidadMedida)
			.then(unidadMedida => {
				if (unidadMedida.length) {
					unidadMedida = unidadMedida[0];
					let producto = new Producto(sku, nombreProducto, unidadMedida.idUnidadMedida);
					producto.save()
						.then(() => {
							res.json({ error: false, message: 'Product created!' });
						})
						.catch(err => {
							if (err.code == 'ER_DUP_ENTRY') {
								res.status(400).json({ error: true, message: 'SKU duplicated' })
							} else {
								console.error(err);
								res.status(500).json({ error: true, message: 'An internal error has occurred' })
							}

						})
				} else {
					res.status(400).json({ error: true, message: 'Bad request, brand not found' });
				}
			})
			.catch(err => {
				console.error(err);
				res.status(500).json({ error: true, message: 'An internal error has occurred' })
			})
	}
}

exports.updateProducto = (req, res) => {
	let { idProducto, sku, nombreProducto, unidadMedida } = req.body;
	if (!idProducto || !sku || !nombreProducto || !unidadMedida) {
		res.status(400).json({ error: true, message: 'Bad request' });
	} else {
		Producto.get(idProducto)
			.then(producto => {
				if (producto.length) {
					UnidadMedida.getUnidadMedida(unidadMedida)
						.then(unidadMedida => {
							if (unidadMedida.length) {
								unidadMedida = unidadMedida[0];
								let producto = new Producto(sku, nombreProducto, unidadMedida.idUnidadMedida);
								Producto.update(idProducto, producto)
									.then(() => {
										res.json({ error: false, message: 'Product updated!' });
									})
									.catch(err => {
										console.error(err);
										res.STATUS(500).json({ error: true, message: 'An internal error has occurred' })
									})
							} else {
								res.status(400).json({ error: true, message: 'Bad request' });
							}
						})
						.catch(err => {
							console.error(err);
							res.status(500).json({ error: true, message: 'An internal error has occurred' })
						})
				} else {
					res.json({ error: false, message: 'Product not found.' })
				}
			})
			.catch(err => {
				console.error(err);
				res.status(500).json({ error: true, message: 'An internal error has occurred' })
			})
	}
}

exports.deleteProducto = (req, res) => {
	let { idProducto } = req.body;
	if (!idProducto) {
		res.json(400).json({ error: true, message: 'Bad request' });
	} else {
		Producto.delete(idProducto)
			.then(() => {
				res.json({ error: false, message: 'Product deleted!' });
			})
			.catch(err => {
				console.error(err);
				res.status(500).json({ error: true, message: 'An internal error has occurred' })
			})
	}
}