const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const session = require('express-session');
const flash = require('connect-flash');
const logger = require('morgan');
const passport = require('passport');
const passportOneSessionPerUser = require('passport-one-session-per-user');
const helmet = require('helmet');


passport.use(new passportOneSessionPerUser());

let app = express();

require('dotenv').load();

//require('./config/passport')(passport);

const indexRouter = require('./routes/index');
const adminRouter = require('./routes/admin');
const apiRouter = require('./routes/api');


// view engine setup
app.set('views', path.join(__dirname, 'views'));

app.set('view engine', 'pug');

// Helmet
app.use(helmet.dnsPrefetchControl());
app.use(helmet.frameguard({ action: 'sameorigin' }));
app.use(helmet.hidePoweredBy());
const fifteenDaysInSeconds = 1296000
app.use(helmet.hsts({
  maxAge: fifteenDaysInSeconds
}));
app.use(helmet.ieNoOpen());
app.use(helmet.xssFilter());
// End Helmet

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(session({
  secret: 's3cr3tFerreteria',
  resave: true,
  saveUninitialized: true,
  httpOnly: true,
}));

// Passport middleware

app.use(passport.initialize());
app.use(passport.session());
app.use(passport.authenticate('passport-one-session-per-user')); // One user per session
app.use(flash());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());


app.use(express.static(path.join(__dirname, 'public')));


app.use(function (req, res, next) {
  res.locals.user = req.user || null;
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  next();
});


app.use('/', indexRouter);
app.use('/admin', adminRouter);
app.use('/api', apiRouter);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});




module.exports = app;
