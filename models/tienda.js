const con = require('../controllers/db');

class Tienda{
    static getTiendas(){
        return con.query('SELECT * FROM tienda');
    }
    static getProductoTienda(idProductoTienda, idTienda){
        if(idTienda == undefined){
            return con.query('SELECT * FROM productoTienda WHERE idTienda = ? AND idProducto = ?', [idTienda, idProductoTienda]);
        }else{
            return con.query('SELECT * FROM productoTienda WHERE idProductoTienda = ?', [idProductoTienda])
        }
    }
    static getProductosTienda(idTienda){
        return con.query('SELECT * FROM productoTienda pT '+
                        'JOIN tienda t ON t.idTienda = pT.idTienda '+
                        'WHERE pT.estadoProducto = \'1\' AND t.idTienda = ?', [idTienda])
    }
    static addProductoTienda(idTienda, idProducto, precioReferencial){
        return con.query('INSERT INTO productoTienda(idTienda, idProducto, precioReferencialUnitario) '+
                        'VALUES(?, ? ,?)', [idTienda, idProducto, precioReferencial]);
    }
    static deleteProductoTienda(idTienda, idProducto){
        return con.query('UPDATE INTO productoTienda SET estadoProducto = \'0\' '+
                        'WHERE idTienda = ? AND idProducto = ?', [idTienda, idProducto]);
    }
    static updateProductoTienda(idProductoTienda, precioReferencialUnitario){
        return con.query('UPDATE INTO productoTienda SET precioReferencialUnitario = ? WHERE idProductoTienda = ?', [precioReferencialUnitario, idTienda]);
    }
}

module.exports = Tienda;