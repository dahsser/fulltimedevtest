const con = require('../controllers/db');

class UnidadMedida {
	static getUnidadMedida(abbreviation) {
		return con.query('SELECT * FROM unidadMedida WHERE abbreviation = ?', [abbreviation]);
	}
	static getUnidadMedidaAll() {
		return con.query('SELECT * FROM unidadMedida');
	}
}

module.exports = UnidadMedida;