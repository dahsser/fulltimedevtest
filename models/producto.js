const con = require('../controllers/db');

class Producto {
	constructor(sku, nombreProducto, idUnidadMedida) {
		this.sku = sku;
		this.nombreProducto = nombreProducto;
		this.idUnidadMedida = idUnidadMedida
	}
	static get(idProducto) {
		return con.query('SELECT p.*, uM.abbreviation FROM producto p JOIN unidadMedida uM ON p.idUnidadMedida = uM.idUnidadMedida WHERE idProducto = ?', [idProducto]);
	}
	static getAll() {
		return con.query('SELECT p.*, uM.abbreviation FROM producto p JOIN unidadMedida uM ON p.idUnidadMedida = uM.idUnidadMedida ORDER BY createdAt DESC');
	}

	save() {
		return con.query('INSERT INTO producto(SKU, nombreProducto, idUnidadMedida) ' +
			'VALUES(?, ?, ?)', [this.sku, this.nombreProducto, this.idUnidadMedida]);
	}
	static update(idProducto, prodObj) {
		return con.query('UPDATE producto SET SKU = ?, nombreProducto = ?, idUnidadMedida = ? ' +
			'WHERE idProducto = ?', [prodObj.sku, prodObj.nombreProducto, prodObj.idUnidadMedida, idProducto]);
	}
	static delete(idProducto) {
		return con.query('CALL SP_deleteProduct(?)', [idProducto]);
	}
}

module.exports = Producto;